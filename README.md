# myVSC

Visual Studio Code extension export/import script.

## Import

The import script will import the Visual Studio Code extension.\n
To run it:
```bash
curl https://gitlab.com/myscripts12/myvsc/-/raw/master/import.sh | bash
```
