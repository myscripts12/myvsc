#!/bin/bash/
# This script must be run with bash (dash for instance will not accept "<<<")

OS=$(grep "^ID=" /etc/os-release)
OS=$(sed -e 's#.*=\(\)#\1#' <<<$OS)
OS=$(sed 's/"//g' <<<$OS)

printf "The  current OS is $OS\n"

# Check if the OS corresponds to a known one, if not exit with error
[[ $OS != "manjaro" && $OS != "arch" && $OS != "ubuntu" && $OS != "debian" ]] && ( printf "OS not supported\n" ; exit 1 )

printf "\n########## Install Visual Studio Code ##########\n"
case $OS in
manjaro|arch)
    paru -S --noconfirm visual-studio-code-bin
    ;;
ubuntu|debian)
    sudo snap -qq install -y code
    ;;
esac

printf "\n########## Install Visual Studio Code plugins ##########\n"

code --install-extension budparr.language-hugo-vscode
code --install-extension bungcip.better-toml
code --install-extension eamodio.gitlens
code --install-extension erd0s.terraform-autocomplete
code --install-extension esbenp.prettier-vscode
code --install-extension foxundermoon.shell-format
code --install-extension haaaad.ansible
code --install-extension hashicorp.terraform
code --install-extension ms-azuretools.vscode-docker
code --install-extension ms-python.python
code --install-extension ms-toolsai.jupyter
code --install-extension ms-vscode-remote.remote-ssh
code --install-extension ms-vscode-remote.remote-ssh-edit
code --install-extension ms-vscode.cpptools
code --install-extension puppet.puppet-vscode
code --install-extension rusnasonov.vscode-hugo
code --install-extension shakram02.bash-beautify
code --install-extension vsciot-vscode.vscode-arduino
code --install-extension vscoss.vscode-ansible
code --install-extension wingrunr21.vscode-ruby
code --install-extension yzhang.markdown-all-in-one
